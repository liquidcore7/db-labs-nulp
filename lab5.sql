DROP OWNED BY am_guest;
DROP USER IF EXISTS am_guest;
CREATE USER am_guest WITH PASSWORD 'amguest';
GRANT CONNECT ON DATABASE excursion TO am_guest;

DROP OWNED BY AM_ADMIN;
DROP ROLE IF EXISTS AM_ADMIN;
CREATE ROLE AM_ADMIN;
GRANT ALL ON DATABASE excursion TO AM_ADMIN;

DROP OWNED BY AM_EDIT;
DROP ROLE IF EXISTS AM_EDIT;
CREATE ROLE AM_EDIT;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO AM_EDIT;

DROP OWNED BY AM_MOD;
DROP ROLE IF EXISTS AM_MOD;
CREATE ROLE AM_MOD;
GRANT SELECT, DELETE ON ALL TABLES IN SCHEMA public TO AM_MOD;

DROP OWNED BY AM_CONTENT_MANAGER;
DROP ROLE IF EXISTS AM_CONTENT_MANAGER;
CREATE ROLE AM_CONTENT_MANAGER;
GRANT SELECT, INSERT ON person, bus, trip_point, trip TO AM_CONTENT_MANAGER;

DROP OWNED BY AM_READER;
DROP ROLE IF EXISTS AM_READER;
CREATE ROLE AM_READER;
GRANT SELECT ON trip, driver, trip_point, assigned_trip, bus TO AM_READER;



GRANT AM_CONTENT_MANAGER, AM_MOD TO am_guest;
REVOKE AM_MOD FROM am_guest;
GRANT SELECT, UPDATE ON payment TO am_guest;
REVOKE SELECT, UPDATE ON payment FROM am_guest;
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM am_guest;



CREATE TABLE IF NOT EXISTS audit (
    id BIGSERIAL PRIMARY KEY,
    username varchar(255) NOT NULL,
    timestamp timestamp NOT NULL DEFAULT now(),
    action varchar(255) not null
);

DROP FUNCTION IF EXISTS log_activity();
CREATE OR REPLACE FUNCTION log_activity2() RETURNS event_trigger AS
$$
BEGIN
  INSERT INTO audit (username, action) VALUES (session_user, 'DDL');

END
$$ LANGUAGE plpgsql;

DROP EVENT TRIGGER IF EXISTS activity_logger2;
CREATE EVENT TRIGGER activity_logger2 ON ddl_command_start EXECUTE PROCEDURE log_activity2();

CREATE TABLE test (
    id BIGSERIAL PRIMARY KEY
);

DROP TABLE test;