-- SELECT WITH ORDER: non-bus drivers
SELECT last_name, first_name, driver_license
  FROM person
  WHERE driver_license IS NOT NULL AND driver_license != 'D'
  ORDER BY (last_name, first_name);

-- SELECT WITH NAMED EXPRESSION: calculated price for assigned trips
SELECT date, passengers_cnt, price(trip_id) AS trip_price
  FROM assigned_trip
  ORDER BY date DESC;

-- SELECT WITH INNER JOIN: trips longer than 1 day with source/destination point names
SELECT t.title, t.distance, t.duration, d.title AS destination_name, s.title AS source_name
  FROM trip AS t
  INNER JOIN trip_point AS d ON t.destination = d.id
  INNER JOIN trip_point AS s ON t.source      = s.id
  WHERE t.duration > 24 * 60;

-- SELECT WITH CROSS JOIN: possible bus/driver combinations
SELECT b.manufacturer, b.model, d.first_name, d.last_name
  FROM bus AS b
  CROSS JOIN driver AS d;

-- SELECT WHERE IS NULL: list all non-drivers
SELECT *
  FROM person
  WHERE driver_license IS NULL
  ORDER BY (last_name, first_name);

-- SELECT WITH AGGREGATES: average payment amount
SELECT avg(amount) AS average_bill
  FROM payment;

-- SELECT WITH GROUP BY: trips count by excursion type
SELECT type, count(*)
  FROM trip
  GROUP BY type;

-- SELECT WITH HAVING: trips with profit > 1000
SELECT t.title
  FROM trip AS t
  JOIN assigned_trip AS a ON t.id = a.trip_id
  JOIN payment       AS p ON a.trip_id = p.trip_id
  GROUP BY t.id
  HAVING sum(p.amount) > 1000;

-- SELECT WITH IN STATEMENT: trips that do start/end in Kiev/Lviv
WITH ukr_airports(name) AS (
    SELECT 'KBP', 'LWO'
)
SELECT id, title
  FROM trip
  WHERE source IN (SELECT id FROM trip_point WHERE city_code IN (SELECT name FROM ukr_airports))
  OR    destination IN (SELECT id FROM trip_point WHERE city_code IN (SELECT name FROM ukr_airports));

-- SELECT WITH SUBQUERY IN HAVING: IDs of trips that have at least one unpaid assigned trip
SELECT at.trip_id
  FROM assigned_trip AS at
  GROUP BY at.trip_id
  HAVING count(*) < count((SELECT 1 FROM payment WHERE at.trip_id=at.id));

-- SELECT WITH CASE: transform coordinates sign for trip points
-- ASSUMING negative longtitude stands for West; negative latitude stands for South
SELECT title, city_code, concat(
    CASE
      WHEN n_coord >= 0.0 THEN format('%s N', to_char(n_coord, '099.99'))
      ELSE                     format('%s S', to_char(-n_coord, '099.99'))
    END,
    ', ',
    CASE
      WHEN e_coord >= 0.0 THEN format('%s E', to_char(e_coord, '0999.99'))
      ELSE                     format('%s W', to_char(-e_coord, '0999.99'))
    END
  ) AS coordinates
  FROM trip_point;

-- SELECT WITH INTERSECT/EXCEPT: assigned trip ids that were paid in a three-months-period having at least 40 passengers
SELECT trip_id
  FROM payment
  WHERE paid_on > now() - make_interval(months := 3)
   INTERSECT
  SELECT trip_id FROM assigned_trip WHERE passengers_cnt >= 40
    EXCEPT
  SELECT trip_id FROM assigned_trip WHERE date > now();

-- INSERT VALUES: add person
INSERT INTO person (first_name, last_name, driver_license)
  VALUES ('Petro', 'Topylko', 'B') ON CONFLICT DO NOTHING;

-- INSERT MULTIPLE ROWS: add buses
INSERT INTO bus (manufacturer, model, license_plate, capacity, fuel_consumption)
  VALUES ('Ikarus', '280', 'BC1234CB', 40, 22.1),
         ('Volvo', '9700', 'AA1101AA', 60, 24.0) ON CONFLICT DO NOTHING;

-- UPDATE ONE TABLE: remove driver license
UPDATE person SET driver_license=NULL WHERE first_name='Petro' AND last_name='Topylko';

-- UPDATE MULTIPLE ROWS WITH SUBQUERIES: add source to trip titles
UPDATE trip SET title=concat(
    title,
    ' (from ',
    (SELECT title FROM trip_point AS tp WHERE tp.id=source LIMIT 1),
    ')');

-- DELETE ALL: reevaluate drivers
-- DELETE FROM driver;

-- DELETE WITH SUBQUERIES: invalidate unpaid assigned trips
DELETE FROM assigned_trip AS at
  WHERE at.date > (now() + make_interval(weeks := 1))
  AND at.id NOT IN(
    SELECT trip_id FROM payment
);


-- AVAILABLE EXCURSIONS
SELECT
       t.id,
       t.title,
       t.duration,
       t.distance,
       t.capacity,
       t.type,
       estimated_price(t.id),
       (SELECT title FROM trip_point WHERE id=t.source LIMIT 1) AS source,
       (SELECT title FROM trip_point WHERE id=t.destination LIMIT 1) AS destination
  FROM trip AS t;


-- BUS LOAD
PREPARE bus_load(timestamp, timestamp) AS (
  SELECT
    t.title,
    t.type,
    a.date AS start_date,
    a.date + make_interval(secs := t.duration * 60) AS end_date,
    b.manufacturer,
    b.model,
    d.first_name,
    d.last_name
  FROM assigned_trip AS a
  JOIN trip   AS t ON a.trip_id = t.id
  JOIN bus    AS b ON a.bus_id = b.id
  JOIN driver AS d ON a.driver_id = d.id
  WHERE a.date < $2 AND a.date > $1
);

EXECUTE bus_load(now() - make_interval(months := 3), now() + make_interval(months := 3));

DEALLOCATE monthly_payment_report;

PREPARE monthly_payment_report(int, int) AS (
    SELECT a.date,
           t.title,
           (SELECT first_name FROM person WHERE id=p.paid_by) AS paid_by_fname,
           (SELECT last_name  FROM person WHERE id=p.paid_by) AS paid_by_lname,
           sum(p.amount) OVER() AS total_profit

    FROM payment AS p
    JOIN assigned_trip AS a ON p.trip_id = a.id
    JOIN trip          AS t ON a.trip_id = t.id
    WHERE date_part('month', p.paid_on)::int = $1
    AND   date_part('year',  p.paid_on)::int = $2
);

EXECUTE monthly_payment_report(10, 2019);


PREPARE invoice_by_requesting_entity(bigint) AS (
    SELECT p.first_name,
           p.last_name,
           t.title,
           t.type,
           pa.amount,
           pa.paid_on,
           sum(pa.amount) OVER () AS total_paid

    FROM person AS p
    JOIN payment       AS pa ON pa.paid_by = p.id
    JOIN assigned_trip AS at ON pa.trip_id = at.id
    JOIN trip          AS t  ON at.trip_id = t.id

    WHERE p.id=$1
);


-- SELECT id FROM person WHERE first_name='Petro' AND last_name='Melnyk';
EXECUTE invoice_by_requesting_entity(1);