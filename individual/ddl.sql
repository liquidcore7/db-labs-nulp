CREATE TABLE IF NOT EXISTS wares (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT
);

CREATE TABLE IF NOT EXISTS people (
    id BIGSERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS providers (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT
);

CREATE TABLE IF NOT EXISTS wares_by_providers (
    provider_id    BIGINT NOT NULL REFERENCES providers,
    ware_id        BIGINT NOT NULL REFERENCES wares,
    provider_price FLOAT  NOT NULL,

    PRIMARY KEY (provider_id, ware_id)
);

CREATE TYPE JOB_POSITION       AS ENUM ('manager', 'salesman');
CREATE TYPE TRADING_POINT_TYPE AS ENUM ('market', 'mall', 'store', 'kiosk');


CREATE TABLE IF NOT EXISTS trading_points (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT,
    type TRADING_POINT_TYPE NOT NULL,
    size_index FLOAT NOT NULL CHECK ( size_index >= 1.0 )
);

-- DROP TABLE IF EXISTS employment;
CREATE TABLE IF NOT EXISTS employment (
    trading_point_id BIGINT       NOT NULL REFERENCES trading_points,
    employee_id      BIGINT       NOT NULL REFERENCES people,
    position         JOB_POSITION NOT NULL,
    salary           FLOAT        NOT NULL CHECK ( salary > 130.0 ),

    PRIMARY KEY (trading_point_id, employee_id)
);

CREATE TABLE IF NOT EXISTS wares_by_trading_points (
    trading_point_id BIGINT NOT NULL REFERENCES trading_points,
    ware_id          BIGINT NOT NULL REFERENCES wares,
    items_cnt        INT NOT NULL CHECK ( items_cnt > 0 ),
    reseller_price   FLOAT NOT NULL,
    bought_on        TIMESTAMP NOT NULL,

    PRIMARY KEY (trading_point_id, ware_id)
);


-- DROP TABLE IF EXISTS sell_log;
CREATE TABLE IF NOT EXISTS sell_log (
    id BIGSERIAL PRIMARY KEY,
    trading_point_id BIGINT NOT NULL REFERENCES trading_points,
    customer_id      BIGINT NOT NULL REFERENCES people,
    ware_id          BIGINT NOT NULL REFERENCES wares,
    bought_on        TIMESTAMP NOT NULL DEFAULT now(),
    sold_by          BIGINT NOT NULL REFERENCES people,
    quantity         INT NOT NULL DEFAULT 1 CHECK ( quantity >= 1 )
);


