-- 1
PREPARE customers_by_wares(BIGINT, TSRANGE) AS (
    SELECT p.id, p.first_name, p.last_name, sl.bought_on
    FROM sell_log AS sl
    INNER JOIN people AS p ON sl.customer_id = p.id
    WHERE sl.ware_id=$1 AND sl.bought_on <@ $2
);
EXECUTE customers_by_wares(4, tsrange( (now() - make_interval(years := 2))::timestamp, now()::timestamp));

-- 2
PREPARE customers_by_min_ware_count(BIGINT, INT) AS (
    SELECT p.id, p.first_name, p.last_name
    FROM people AS p
    WHERE (SELECT count(*) FROM sell_log WHERE customer_id=p.id AND ware_id=$1) > $2
);
EXECUTE customers_by_min_ware_count(8, 1);

-- 3
PREPARE wares_by_trading_point(BIGINT) AS (
    SELECT w.id, w.title, w.description
    FROM wares_by_trading_points AS w_tp
    INNER JOIN wares AS w ON w_tp.ware_id = w.id
    WHERE w_tp.trading_point_id = $1
);
EXECUTE wares_by_trading_point(3);

-- 4
PREPARE prices_by_ware(BIGINT) AS (
    SELECT tp.title, tp.type, w_tp.reseller_price, w_tp.items_cnt
    FROM wares_by_trading_points AS w_tp
    INNER JOIN trading_points AS tp ON w_tp.trading_point_id = tp.id
    WHERE w_tp.ware_id = $1
    ORDER BY w_tp.reseller_price ASC, w_tp.items_cnt DESC
);
EXECUTE prices_by_ware(8);

-- 5
PREPARE prices_by_ware_and_type(BIGINT, TRADING_POINT_TYPE) AS (
    SELECT tp.title, w_tp.reseller_price, w_tp.items_cnt
    FROM wares_by_trading_points AS w_tp
    INNER JOIN trading_points AS tp ON w_tp.trading_point_id = tp.id
    WHERE w_tp.ware_id = $1 AND tp.type = $2
    ORDER BY w_tp.reseller_price ASC, w_tp.items_cnt DESC
);
EXECUTE prices_by_ware_and_type(8, 'store');

-- 6
PREPARE price_by_trading_point(BIGINT, BIGINT) AS (
    SELECT w_tp.reseller_price, w_tp.items_cnt
    FROM wares_by_trading_points AS w_tp
    WHERE w_tp.ware_id = $1 AND w_tp.trading_point_id = $2
);
EXECUTE price_by_trading_point(8, 1);

-- 7
PREPARE sold_by_seller(BIGINT, TSRANGE) AS (
    SELECT tp.title, tp.type, w.title, w.description, w_tp.reseller_price, sl.quantity, sl.bought_on
    FROM sell_log AS sl
    INNER JOIN wares AS w ON sl.ware_id = w.id
    INNER JOIN wares_by_trading_points AS w_tp
        ON w.id = w_tp.ware_id AND sl.trading_point_id = w_tp.trading_point_id
    INNER JOIN trading_points AS tp ON sl.trading_point_id = tp.id
    WHERE sl.sold_by = $1 AND sl.bought_on <@ $2
);
EXECUTE sold_by_seller(6, tsrange(
    (now() - make_interval(days := 2))::timestamp, now()::timestamp
));

-- 8
PREPARE sold_by_ware(BIGINT, TSRANGE) AS (
    SELECT tp.title, tp.type, sl.bought_on, sl.quantity
    FROM sell_log AS sl
    INNER JOIN trading_points AS tp ON sl.trading_point_id = tp.id
    WHERE sl.ware_id = $1 AND sl.bought_on <@ $2
);
EXECUTE sold_by_ware(8, tsrange(
    (now() - make_interval(days := 3))::timestamp, now()::timestamp
));

-- 9
PREPARE seller_salaries AS (
    SELECT p.first_name, p.last_name, e.position, round(e.salary::numeric, 2)
    FROM employment AS e
    INNER JOIN people AS p ON e.employee_id = p.id
);
EXECUTE seller_salaries;

-- 10
PREPARE supplies_by_ware_and_provider(BIGINT, BIGINT, TSRANGE) AS (
    SELECT tp.title, tp.type, w_tp.items_cnt, w_tp.bought_on
    FROM wares_by_providers AS w_p
    INNER JOIN wares_by_trading_points AS w_tp ON w_p.ware_id = w_tp.ware_id
    INNER JOIN trading_points AS tp ON w_tp.trading_point_id = tp.id
    WHERE w_p.ware_id = $1 AND w_p.provider_id = $2 and w_tp.bought_on <@ $3
);
EXECUTE supplies_by_ware_and_provider(8, 2, tsrange(
    (now() - make_interval(years := 5))::timestamp, (now() + make_interval(years := 5))::timestamp
));

-- 11
PREPARE sales_to_size_ratio AS (
    SELECT round(((
        SELECT sum(sl.quantity * w_tp.reseller_price)
        FROM sell_log AS sl
        INNER JOIN wares_by_trading_points AS w_tp
            ON w_tp.trading_point_id = sl.trading_point_id
        WHERE sl.trading_point_id = tp.id
    ) / tp.size_index)::numeric, 2) AS sale_to_size_ratio
    FROM trading_points AS tp
);
EXECUTE sales_to_size_ratio;

-- 12
PREPARE marginality(TSRANGE) AS (
    SELECT round(((
        SELECT sum(sl.quantity * w_tp.reseller_price)
        FROM sell_log AS sl
        INNER JOIN wares_by_trading_points AS w_tp
            ON w_tp.trading_point_id = sl.trading_point_id
        WHERE sl.trading_point_id = tp.id AND sl.bought_on <@ $1
    ) / (
        SELECT sum(w_p.provider_price)
        FROM wares_by_trading_points AS w_tp
        INNER JOIN wares_by_providers AS w_p ON w_tp.ware_id = w_p.ware_id
        WHERE w_tp.trading_point_id = tp.id AND w_tp.bought_on <@ $1
    ))::numeric, 2) AS marginality
    FROM trading_points AS tp
);
EXECUTE marginality(tsrange(
    (now() - make_interval(months := 12))::timestamp, (now() + make_interval(months := 0))::timestamp
));

-- 13
PREPARE customer_report_by_ware(BIGINT, TSRANGE) AS (
    SELECT p.first_name, p.last_name, sl.quantity, sl.bought_on
    FROM sell_log AS sl
    INNER JOIN people AS p ON sl.customer_id = p.id
    WHERE sl.ware_id = $1 AND sl.bought_on <@ $2
);
EXECUTE customer_report_by_ware(8, tsrange(
    (now() - make_interval(days := 1))::timestamp, (now() + make_interval(days := 2))::timestamp
));

-- 14
PREPARE top_customers(BIGINT) AS (
    SELECT p.first_name, p.last_name, sum(sl.quantity)
    FROM sell_log AS sl
    INNER JOIN people AS p ON sl.customer_id = p.id
    WHERE sl.trading_point_id = $1
    GROUP BY p.id
    ORDER BY sum(sl.quantity) DESC
);
EXECUTE top_customers(1);

-- 15
PREPARE ware_flow(TSRANGE) AS (
    SELECT w.title, w.description, sl.bought_on AS sold_on, sl.quantity, w_tp.bought_on, w_tp.items_cnt
    FROM sell_log AS sl
    INNER JOIN wares AS w ON sl.ware_id = w.id
    INNER JOIN wares_by_trading_points AS w_tp
        ON sl.ware_id = w_tp.ware_id AND sl.trading_point_id = w_tp.trading_point_id
    WHERE sl.bought_on <@ $1 OR w_tp.bought_on <@ $1
);
EXECUTE ware_flow(tsrange(
    (now() - make_interval(months := 1))::timestamp, (now() + make_interval(days := 2))::timestamp
));

