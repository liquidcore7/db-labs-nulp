INSERT INTO wares (id, title, description) VALUES
(1, 'Lvivske 1715', 'Disgusting beer from Lviv'),
(2, 'Plastic can', '0.33l plastic can'),
(3, 'Zippo lighter', 'Metallic zippo lighter using kerosene'),
(4, 'Milk Selyanske', '1l milk pack'),
(5, 'DLink router', 'DLink DIR-300'),
(6, 'Pizza from New York Street Pizza', '40cm florentina'),
(7, 'Graff tobacco', '30g tobacco with chocolate taste'),
(8, 'Jaffa juice', '1l of orange juice');


INSERT INTO people (id, first_name, last_name) VALUES
(1, 'Petro', 'Topolya'),
(2, 'Yurii', 'Markov'),
(3, 'Anastasiia', 'Shevchenko'),
(4, 'Yurii', 'Pankov'),
(5, 'Van', 'Pechorin'),
(6, 'Igor', 'Repushko'),
(7, 'Olena', 'Illienko'),
(8, 'Roman', 'Kozak'),
(9, 'Olga', 'Muzychko'),
(10, 'Oksana', 'Tank'),
(11, 'Andrii', 'Kolesnikov'),
(12, 'Anna', 'Chepiga'),
(13, 'Ivan', 'Mayorenko');


INSERT INTO providers (id, title, description) VALUES
(1, 'AliExpress', 'AliBaba group marketplace'),
(2, 'Green lion', 'Universal marketplace in Lviv'),
(3, 'Ukrtelecom', 'Ukrainian telecom provider'),
(4, 'New York Street Pizza', 'Pizza network in Ukraine');


INSERT INTO wares_by_providers (provider_id, ware_id, provider_price) VALUES
(1, 2, 0.0005),
(1, 3, 20.0),
(1, 5, 15.0),
(2, 1, 0.95),
(2, 2, 0.0006),
(2, 4, 1.65),
(2, 7, 4.2),
(2, 8, 1.3),
(3, 5, 25.0),
(4, 1, 1.1),
(4, 6, 6.5),
(4, 8, 1.5);


INSERT INTO trading_points (id, title, description, type, size_index) VALUES
(1, 'Blyzenko', 'Ukrainian food shops network', 'store', 2.5),
(2, 'Rukavychka', 'Ukrainian food shops network', 'store', 3.0),
(3, 'Silpo', 'Ukrainian mall network', 'mall', 8.0),
(4, 'Ukrtelecom office', 'Office of ukrtelecom telecom provider', 'store', 4.0),
(5, 'New York Street Pizza', 'Ukrainian pizza restaurants network', 'store', 2.0);


INSERT INTO employment (trading_point_id, employee_id, position, salary) VALUES
(1, 3, 'manager', 700.00),
(2, 2, 'manager', 850.00),
(3, 3, 'manager', 1450.00),
(4, 4, 'manager', 1700.00),
(5, 5, 'manager', 900.00),
(1, 6, 'salesman', 700.00 / 3.0),
(2, 7, 'salesman', 850.00 / 3.0),
(3, 8, 'salesman', 1450.00 / 3.0),
(4, 9, 'salesman', 1700.00 / 3.0),
(5, 10, 'salesman', 900.00 / 3.0);


INSERT INTO wares_by_trading_points (trading_point_id, ware_id, items_cnt, reseller_price, bought_on) VALUES
(1, 1, 50, 1.4, now() - make_interval(weeks := 2)),
(1, 2, 750, 0.0007, now() - make_interval(months := 1)),
(1, 4, 30, 1.8, now() - make_interval(days := 1)),
(1, 8, 35, 1.6, now() - make_interval(weeks := 1)),
(2, 1, 70, 1.35, now() + make_interval(days := 2)),
(2, 2, 1250, 0.0006, now() - make_interval(years := 3)),
(2, 4, 60, 1.85, now()),
(2, 8, 75, 1.7, now() - make_interval(days := 5)),
(3, 1, 250, 1.5, now() - make_interval(weeks := 2)),
(3, 2, 350, 0.0007, now() - make_interval(months := 2)),
(3, 4, 90, 1.9, now() + make_interval(days := 3)),
(3, 8, 135, 1.75, now() - make_interval(days := 3)),
(3, 7, 30, 4.5, now() - make_interval(weeks := 2)),
(3, 3, 20, 25.5, now() - make_interval(years := 2)),
(4, 5, 8, 30.0, now() - make_interval(months := 6)),
(5, 1, 40, 1.6, now() - make_interval(days := 6)),
(5, 6, 200, 6.8, now()),
(5, 8, 100, 1.7, now() + make_interval(days := 3));


INSERT INTO sell_log (id, trading_point_id, customer_id, ware_id, bought_on, sold_by, quantity) VALUES
(1, 1, 12, 4, now() - make_interval(days := 1), 6, 2),
(2, 1, 12, 8, now() + make_interval(days := 2), 6, 1),
(3, 1, 13, 1, now() - make_interval(days := 1, hours := 2), 6, 3),
(4, 1, 13, 2, now(), 6, 8),
(5, 2, 11, 4, now() - make_interval(weeks := 1), 7, 1),
(6, 2, 12, 4, now() - make_interval(days := 3), 7, 1),
(7, 3, 11, 3, now() + make_interval(days := 1, hours := 1), 8, 1),
(8, 3, 11, 7, now() - make_interval(days := 3), 8, 1),
(9, 3, 13, 8, now() - make_interval(days := 2, hours := 5), 8, 2),
(10, 3, 13, 4, now() - make_interval(days := 1, hours := 3), 8, 1),
(11, 4, 12, 5, now() - make_interval(days := 4), 9, 1),
(12, 5, 13, 6, now() + make_interval(days := 1, hours := 1), 10, 2),
(13, 5, 13, 8, now(), 10, 1);
