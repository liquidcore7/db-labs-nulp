ALTER TABLE payment ADD IF NOT EXISTS ucr VARCHAR(255);
ALTER TABLE payment ADD IF NOT EXISTS dcr timestamp;
ALTER TABLE payment ADD IF NOT EXISTS ulc VARCHAR(255);
ALTER TABLE payment ADD IF NOT EXISTS dlc timestamp;


CREATE OR REPLACE FUNCTION payment_on_create() RETURNS TRIGGER AS
    $$
    BEGIN
      NEW.ucr := current_user;
      NEW.dcr := now();
      RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;
CREATE TRIGGER payment_on_create_add_metadata BEFORE INSERT ON payment FOR EACH ROW EXECUTE PROCEDURE payment_on_create();


CREATE OR REPLACE FUNCTION payment_on_update() RETURNS TRIGGER AS
    $$
    BEGIN
      NEW.ulc := current_user;
      NEW.dlc := now();
      RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;
CREATE TRIGGER payment_on_update_refresh_metadata BEFORE INSERT OR UPDATE ON payment FOR EACH ROW EXECUTE PROCEDURE payment_on_update();



ALTER TABLE bus ADD IF NOT EXISTS tech_review_id bigint;
CREATE UNIQUE INDEX IF NOT EXISTS tech_review_unique_idx ON bus (tech_review_id);

CREATE TABLE IF NOT EXISTS last_bigint(value bigint UNIQUE NOT NULL);
INSERT INTO last_bigint (value) VALUES (0);

CREATE OR REPLACE FUNCTION next_bigint() RETURNS TRIGGER AS
    $$
    BEGIN
        NEW.tech_review_id := (SELECT value FROM last_bigint LIMIT 1);
        UPDATE last_bigint set value = value + 1;
        RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;
CREATE TRIGGER generate_next_id_on_bus_insert BEFORE INSERT ON bus FOR EACH ROW EXECUTE PROCEDURE next_bigint();

-- insert into bus (manufacturer, model, license_plate, capacity, fuel_consumption) VALUES ('Scania', 'Model X', 'BC2213AC', 25, 11.9);

CREATE OR REPLACE FUNCTION ensure_bus_enough_capacity() RETURNS TRIGGER AS
    $$
    DECLARE
        bus_capacity int;
    BEGIN
        bus_capacity := (SELECT capacity FROM bus WHERE id = new.bus_id);
        IF (bus_capacity < new.passengers_cnt) THEN
            RAISE EXCEPTION '% places needed for this trip, but the picked bus has only % available', new.passengers_cnt, bus_capacity;
        END IF;
        RETURN new;
    END;
    $$ LANGUAGE plpgsql;
CREATE TRIGGER ensure_bus_enough_capacity_on_trip_assign BEFORE INSERT OR UPDATE ON assigned_trip FOR EACH ROW EXECUTE PROCEDURE ensure_bus_enough_capacity();


CREATE OR REPLACE FUNCTION ensure_no_debt() RETURNS TRIGGER AS
    $$
    DECLARE
        outdated_payments_cnt int;
    BEGIN
        outdated_payments_cnt := (
            (SELECT count(*) FROM assigned_trip WHERE date + make_interval(months := 1) < now() AND id NOT IN (
                SELECT trip_id FROM payment WHERE paid_by=new.requested_by
            ))
        );
        IF (outdated_payments_cnt > 0) THEN
            RAISE EXCEPTION 'Unable to book a trip: this person has % pending payments', outdated_payments_cnt;
        END IF;
        RETURN new;
    END;
    $$ LANGUAGE plpgsql;
CREATE TRIGGER ensure_payer_has_no_debt_on_trip_assign BEFORE INSERT OR UPDATE ON assigned_trip FOR EACH ROW EXECUTE PROCEDURE ensure_no_debt();


CREATE OR REPLACE FUNCTION ensure_no_conflicting_trips() RETURNS TRIGGER AS
    $$
    DECLARE
        conflicting_bus_trips int;
        conflicting_driver_trips int;
    BEGIN
        conflicting_bus_trips := (
            SELECT count(*)
            FROM assigned_trip AS a
            JOIN trip AS t on a.trip_id = t.id
            WHERE bus_id = new.bus_id
            AND tsrange(a.date, a.date + make_interval(mins := t.duration)) && tsrange(new.date, new.date + (SELECT make_interval(mins := duration) FROM trip WHERE id = new.trip_id))
        );

        conflicting_driver_trips := (
            SELECT count(*)
            FROM assigned_trip AS a
            JOIN trip AS t on a.trip_id = t.id
            WHERE a.driver_id = new.driver_id
            AND tsrange(a.date, a.date + make_interval(mins := t.duration)) && tsrange(new.date, new.date + (SELECT make_interval(mins := duration) FROM trip WHERE id = new.trip_id))
        );

        IF (conflicting_bus_trips > 0) THEN
            RAISE EXCEPTION 'Unable to book a trip: selected bus is busy in that interval';
        END IF;
        IF (conflicting_driver_trips > 0) THEN
            RAISE EXCEPTION 'Unable to book a trip: selected driver is busy in that interval';
        END IF ;
        RETURN new;
    END;
    $$ LANGUAGE plpgsql;
CREATE TRIGGER ensure_bus_driver_available_on_trip_assign BEFORE INSERT OR UPDATE ON assigned_trip FOR EACH ROW EXECUTE PROCEDURE ensure_no_conflicting_trips();



