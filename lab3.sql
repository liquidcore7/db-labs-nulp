CREATE OR REPLACE FUNCTION pay_for_trip(assigned_trip_id bigint, payer_id bigint) RETURNS void LANGUAGE SQL AS
    $$
    INSERT INTO payment (trip_id, paid_by, paid_on, amount) VALUES (assigned_trip_id, payer_id, now(), price(assigned_trip_id)) ON CONFLICT DO NOTHING;
    $$;

CREATE OR REPLACE PROCEDURE pay_for_all_trips(payer_id bigint) LANGUAGE SQL AS
    $$
    SELECT pay_for_trip(id, payer_id) FROM assigned_trip;
    $$;



CALL pay_for_all_trips(1);