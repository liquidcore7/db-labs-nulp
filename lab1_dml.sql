INSERT INTO trip_point (title, city_code, n_coord, e_coord)
VALUES ('Lviv opera', 'LWO', 49.8397, 24.0297);

INSERT INTO trip_point (title, city_code, n_coord, e_coord)
VALUES ('Kiev Boryspil', 'KBP', 50.3382, 30.8939);

INSERT INTO trip_point (title, city_code, n_coord, e_coord)
VALUES ('Warsaw Chopin', 'WAW', 52.1672, 20.9679);



INSERT INTO person (first_name, last_name, driver_license)
VALUES ('Petro', 'Melnyk', NULL);

INSERT INTO person (first_name, last_name, driver_license)
VALUES ('Anastasia', 'Shevchenko', 'B');

INSERT INTO person (first_name, last_name, driver_license)
VALUES ('Serhii', 'Juk', 'D');

INSERT INTO person (first_name, last_name, driver_license)
VALUES ('Yurii', 'Panchenko', 'D');


INSERT INTO bus (manufacturer, model, license_plate, capacity, fuel_consumption)
VALUES ('Scania', 'Ikarus E99', 'BC1337CB', 72, 21.0);

INSERT INTO bus (manufacturer, model, license_plate, capacity, fuel_consumption)
VALUES ('Mercedes-benz', 'Tourismo', 'WZW1356', 55, 19.0);

INSERT INTO bus (manufacturer, model, license_plate, capacity, fuel_consumption)
VALUES ('Ikarus', '870', 'AA1122BC', 35, 16.5);


INSERT INTO trip (title, duration, destination, source, distance, capacity, type)
VALUES ('Warsaw sightseeing from Lviv',
        36 * 60,
        (SELECT id FROM trip_point WHERE city_code = 'WAW'),
        (SELECT id FROM trip_point WHERE city_code = 'LWO'),
        700,
        60,
        'sightseeing'
);

INSERT INTO trip (title, duration, destination, source, distance, capacity, type)
VALUES ('Lviv historical trip from Kiev',
        18 * 60,
        (SELECT id FROM trip_point WHERE city_code = 'LWO'),
        (SELECT id FROM trip_point WHERE city_code = 'KBP'),
        500,
        30,
        'historical'
);



INSERT INTO assigned_trip (trip_id, driver_id, bus_id, requested_by, date, passengers_cnt)
VALUES ((SELECT id FROM trip WHERE title='Lviv historical trip from Kiev'),
        (SELECT id FROM driver LIMIT 1),
        (SELECT id FROM bus WHERE capacity >= 25 LIMIT 1),
        (SELECT id FROM person WHERE first_name = 'Anastasia' AND last_name = 'Shevchenko'),
        now() + make_interval(months := 2),
        25
);


WITH historical_trip(id) AS (
    SELECT a.id
    FROM assigned_trip AS a
        JOIN trip AS t on a.trip_id = t.id
        WHERE t.title = 'Lviv historical trip from Kiev'
    LIMIT 1
) INSERT INTO payment (trip_id, paid_by, paid_on, amount)
  VALUES ((SELECT id FROM historical_trip),
          (SELECT id FROM person LIMIT 1),
          now(),
          price((SELECT id FROM historical_trip))
         ),
         ((SELECT id FROM historical_trip),
          (SELECT id FROM person LIMIT 1),
          now() + make_interval(months := 1, weeks := 3),
          price((SELECT id FROM historical_trip)));



