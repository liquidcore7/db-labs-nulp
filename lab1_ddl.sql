CREATE TABLE trip_point (
    id          BIGSERIAL    PRIMARY KEY,

    title       VARCHAR(255) UNIQUE NOT NULL,
    city_code   VARCHAR(3)   NOT NULL,

    n_coord     FLOAT        NOT NULL,
    e_coord     FLOAT        NOT NULL
);

CREATE TYPE DRIVER_LICENSE_TYPE AS ENUM ('A', 'B', 'C', 'D');


CREATE TABLE person (
    id              BIGSERIAL   PRIMARY KEY,

    first_name      VARCHAR(40) NOT NULL,
    last_name       VARCHAR(55) NOT NULL,

    driver_license  DRIVER_LICENSE_TYPE
);

CREATE TABLE driver AS SELECT * FROM person WHERE driver_license = 'D';
ALTER TABLE driver ADD PRIMARY KEY (id);


CREATE OR REPLACE FUNCTION copy_to_driver()
  RETURNS trigger AS
$BODY$
BEGIN
   IF NEW.driver_license='D' THEN
       INSERT INTO driver(id, first_name, last_name, driver_license)
       VALUES(NEW.id, NEW.first_name, NEW.last_name, 'D');
   END IF;

   RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;


CREATE TRIGGER copy_person_with_d_license_to_driver
  BEFORE INSERT ON person
  FOR EACH ROW
  EXECUTE PROCEDURE copy_to_driver();
-- TODO: create reverse triggers

CREATE TABLE bus (
    id                  BIGSERIAL   PRIMARY KEY,

    manufacturer        VARCHAR(20) NOT NULL,
    model               VARCHAR(70) NOT NULL,

    license_plate       VARCHAR(10) UNIQUE NOT NULL,

    capacity            INT         NOT NULL,
    fuel_consumption    FLOAT       NOT NULL -- liters per km
);

CREATE TYPE TRIP_TYPE AS ENUM ('thematic', 'historical', 'sightseeing');

CREATE TABLE trip (
    id           BIGSERIAL    PRIMARY KEY,

    title        VARCHAR(255) NOT NULL,
    duration     INT          NOT NULL,

    destination  BIGINT       NOT NULL REFERENCES trip_point ON DELETE NO ACTION,
    source       BIGINT       NOT NULL REFERENCES trip_point ON DELETE NO ACTION,
    distance     INT,         -- can be derived from destination and source

    capacity     INT          NOT NULL,
    type         TRIP_TYPE    NOT NULL
);


CREATE TABLE assigned_trip (
    id              BIGSERIAL PRIMARY KEY,

    trip_id         BIGINT          REFERENCES trip,
    driver_id       BIGINT NOT NULL REFERENCES driver,
    bus_id          BIGINT NOT NULL REFERENCES bus,
    requested_by    BIGINT NOT NULL REFERENCES person,

    date            TIMESTAMP NOT NULL,
    passengers_cnt  INT       NOT NULL
);



CREATE OR REPLACE FUNCTION estimated_price(BIGINT) RETURNS FLOAT AS
    $$ SELECT 0.05 * t.distance * t.capacity * t.duration / 60.0 + 25.0 * t.distance * (
               SELECT avg(fuel_consumption) FROM bus AS b
                    WHERE b.capacity >= t.capacity
                      AND b.capacity - t.capacity <= 15
               ) AS price FROM trip AS t WHERE t.id=$1
    $$ LANGUAGE SQL IMMUTABLE RETURNS NULL ON NULL INPUT;



CREATE OR REPLACE FUNCTION price(BIGINT) RETURNS FLOAT AS
    $$ WITH needed_variables(fuel_cons, clients, distance, duration) AS (
        SELECT b.fuel_consumption, a.passengers_cnt, t.distance, t.duration
        FROM assigned_trip AS a
            JOIN bus  AS b on a.bus_id = b.id
            JOIN trip AS t on a.trip_id = t.id
        WHERE a.id=$1
        ) SELECT 25.0 * fuel_cons * distance + 0.05 * distance * clients * duration / 60.0 AS price FROM needed_variables
    $$ LANGUAGE SQL IMMUTABLE RETURNS NULL ON NULL INPUT;




CREATE TABLE payment (
    id            BIGSERIAL   PRIMARY KEY,

    trip_id       BIGINT      NOT NULL REFERENCES assigned_trip ON DELETE NO ACTION,
    paid_by       BIGINT      NOT NULL REFERENCES person        ON DELETE NO ACTION,
    paid_on       TIMESTAMP   NOT NULL,

    amount        FLOAT       NOT NULL CHECK ( round(amount::numeric, 2) = round(price(trip_id)::numeric, 2) )
);

